# createUser

## Function
Creates a local user.  Handy for creating the same user across multiple endpoints.

## Vault
```
---
createUser_username: jdoe
createUser_password: password
createUser_comment: John Doe
createUser_sudoer: True
```

## Tested Environment(s)
RHEL 7 & CentOS 7

## Changelog

**v1.0 (2015-08-28)**
* Original Release
